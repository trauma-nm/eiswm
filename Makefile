# eiswm - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c eiswm.c util.c
OBJ = ${SRC:.c=.o}

all: options eiswm

options:
	@echo eiswm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

eiswm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f eiswm ${OBJ} eiswm-${VERSION}.tar.gz

dist: clean
	mkdir -p eiswm-${VERSION}
	cp -R LICENSE Makefile README config.def.h config.mk\
		eiswm.1 drw.h util.h ${SRC} eiswm.png transient.c dwm-${VERSION}
	tar -cf eiswm-${VERSION}.tar eiswm-${VERSION}
	gzip eiswm-${VERSION}.tar
	rm -rf eiswm-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f eiswm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/eiswm
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < eiswm.1 > ${DESTDIR}${MANPREFIX}/man1/eiswm.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/eiswm.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/eiswm\
		${DESTDIR}${MANPREFIX}/man1/eiswm.1

.PHONY: all options clean dist install uninstall
