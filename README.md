# eiswm - cold window manager

eiswm is based on dwm.

dwm is an extremely fast, small, and dynamic window manager for X.

## Requirements

In order to build eiswm you need the Xlib header files.
- libx11-dev
- libxft-dev
- libxinerama-dev

Dependng on your system, the name of the packages may vary.

Standard launcher and terminal of eiswm are `dmenu` and `xterm`. It is recommended to either have these programs installed or change the default programs in `config.h`

## Installation

If desired, one can edit `config.mk` to change how eiswm is installed. Per default eiswm is installed into
the `/usr/local`.

Afterwards enter the following command to build and install eiswm (if
necessary as root):

```
make clean install

```

## Running eiswm

### Using startx

Add the following line to your .xinitrc to start eiswm using startx:
```
exec eiswm
```
In order to connect eiswm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:
```
DISPLAY=foo.bar:1 exec eiswm
```
(This will start eiswm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:
```
while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
do
  	sleep 1
done &
exec eiswm
```

### Using a greeter

Add a file called `eiswm.desktop` in `/usr/share/xsessions/` with the following content:

```
[Desktop Entry]
Encoding=UTF-8
Name=eiswm
Comment=cold window manager
Exec=eiswm
Icon=eiswm.png
Type=XSession
```

Add an icon from the "icons" folder to `/usr/share/icons/` and rename it to `eiswm.png`

## Configuration

The configuration of eiswm is done by editing config.h and (re)compiling the source code.

## Autostart

eiswm automatically executes a bash file as autostart. Create a file `autostart.sh` in `~/.config/eiswm/`.

## Layout handling

eiswm has 6 different layout options: tiling, floating, monocle, spiral, dwindling and fullscreen.
Instead of the concept of virtual desktops tags exist. Tags can be applied to and removed from windows. Chosing which windows are displayed on the screen is done by choosing the tag(s) to focus on.

```
tiling
+-----------+-----------+
|           |     2     |
|           +-----------|
|           |     3     |
|     1     +-----------+
|           |     4     |
|           +-----------+
|           |     5     |
+-----------+-----------+

monocle
+-----------------------+
|                       |
|                       |
|                       |
|           1           |
|                       |
|                       |
|                       |
+-----------------------+

floating
+-----------------------+
|                       |
| +-----+               |
| |  1  | +-----------+ |
| +----+-----+        | |
|      |  3  |  2     | |
|      +-----+        | |
|         +-----------+ |
+-----------------------+


spiral
+-----------+-----------+  
|           |           |  
|           |     2     |  
|           |           |  
|     1     +--+--+-----+  
|           | 5|-.|     |  
|           +--+--+  3  |  
|           |  4  |     |  
+-----------+-----+-----+  

dwindle
+-----------+-----------+
|           |           |
|           |     2     |
|           |           |
|     1     +-----+-----+
|           |     |  4  |
|           |  3  +--+--+
|           |     | 5|-.|
+-----------+-----+-----+

```
## Keyboard shortcuts

Super: usually the "windows" key

Mod: modifier key, usually the "alt" key

N: number from 1 to and including 9

Super + Space:  open dmenu on the bottom of the screen

Mod + Shift + Return:   open Terminal

Super + Return: open Terminal

Super + b:  toggle bar

Super + j:  focus next window in stack

Super + k:  focus previous window in stack

Super + i:  increase number of windows in master area

Super + d:  decrease number of windows in master area

Super + h:  increase stack area (horizontaly)

Super + l:  decrease stack area (horizontaly)

Super + u:  set focused window as master

Super + Tab:    focus previous workspace

Super + w:  kill focused window

Super + t:  tiling layout

Super + s:  floating layout

Super + r:  spiral layout

Super + Shift + r:  dwindle layout

Super + f:  fullscreen layout (does not yet work as intended)

Super + m: monocle layout

Mod + Space:    set previous layout

Mod + Shift + Space:    toggle floating of focused window

Mod + 0:    view all tags

Mod + Shift + 0: apply all tags to window

Mod + Comma:    focus previous screen

Mod + Period:   focus next screen

Mod + Shift + Comma:    Move window to previous screen

Mod + Shift + Period:   Move window to next screen

Mod + N:    Focus desktop N

Mod + Shift + N: Move window to tag N

Mod + Shift + Ctrl + N: toggle tag N of window

Mod + Ctrl: toggle tag from view

## Mouse bindings

Button1: left click

Button2: mouse wheel

Button3: right click

[Window] + Mod + Button1:   move window (does not yet work as intended)

[Window] + Mod + Button3: resize window (does not yet work as intended)

[Layout Symbol] + Button1:  set previous layout

[Layout Symbol] + Button3:  set Monocle layout

[Tag] + Button1:    focus tag

[Tag] + Button3:    toggle tag

[Tag] + Mod + Button1:  move window to tag

[Tag] + Mod + Button3:  toggle tag for window

[Statustext] + Button2: open terminal

[Window] + Button2: toggle floating

[WindowTitle] + Button2: move window to master

## Tags

Tags intend to provide a similar functionality as virtual desktops. Instead of a virtual desktop containing several windows, one applies a tag to a window. And instead of showing a virtual desktop, one shows all windows with a certain tag applied to them.

Super + Shift + [1..9] applies a tag to a window (similar to moving it to a certain virtual desktop)

Super + [1..9] shows all windows with a certain tag (similar to showing a virtual desktop)

Super + Tab switches between the current and the latest tag (similar to switching between the current and the latest desktop)

Super + Shift + 0 applies all tags to a window (similar to showing a window on all virtual desktops)

Super + 0 view all windows

Super + Control + Shift + [1..9] toggles the tag n of a window (similar to making a window visible on a certian virtual desktop)

Super + Control + [1..9] Toggle visibiltiy of all windwos with tag n

## Sources

eiswm builds up on the work of different people, namely dwm and the gaps, systray and fibonacci patches.

### License of dwm

MIT/X Consortium License

© 2006-2019 Anselm R Garbe <anselm@garbe.ca>

© 2006-2009 Jukka Salmi <jukka at salmi dot ch>

© 2006-2007 Sander van Dijk <a dot h dot vandijk at gmail dot com>

© 2007-2011 Peter Hartlich <sgkkr at hartlich dot com>

© 2007-2009 Szabolcs Nagy <nszabolcs at gmail dot com>

© 2007-2009 Christof Musik <christof at sendfax dot de>

© 2007-2009 Premysl Hruby <dfenze at gmail dot com>

© 2007-2008 Enno Gottox Boland <gottox at s01 dot de>

© 2008 Martin Hurton <martin dot hurton at gmail dot com>

© 2008 Neale Pickett <neale dot woozle dot org>

© 2009 Mate Nagy <mnagy at port70 dot net>

© 2010-2016 Hiltjo Posthuma <hiltjo@codemadness.org>

© 2010-2012 Connor Lane Smith <cls@lubutu.com>

© 2011 Christoph Lohmann <20h@r-36.net>

© 2015-2016 Quentin Rameau <quinq@fifth.space>

© 2015-2016 Eric Pruitt <eric.pruitt@gmail.com>

© 2016-2017 Markus Teich <markus.teich@stusta.mhn.de>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

### Gaps:

https://dwm.suckless.org/patches/gaps/

Carlos Pita (memeplex) carlosjosepita@gmail.com

### Fibonacci:

https://dwm.suckless.org/patches/fibonacci/

Jeroen Schot - schot@a-eskwadraat.nl

Niki Yoshiuchi - nyoshiuchi@gmail.com

### Systray:

https://dwm.suckless.org/patches/systray/

Jan Christoph Ebersbach jceb@e-jc.de

Eon S. Jeon esjeon@hyunmu.am (14343e69cc59)

David Phillips (5ed9c48 (6.1), 20160103, 20180314)

Eric Pruitt (7af4d43 (20160626))

Igor Gevka (cb3f58a (6.2), 20200216)

Michel Boaventura michel.boaventura@protonmail.com (f09418b)

Hritik Vijay hr1t1k@protonmail.com (20210418)

